const request = require('async-request')
import { Product, Utils } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';
import { appSettings } from '../../../config/appSettings';

const url = appSettings.integrations.product.url;

class ProductIntegration {

    getProductsByIds = async (idProducts: string[]) => {
        const urls: string[] = [];
        for (const idProduct of idProducts) {
            urls.push(`${url}/${idProduct}`);
        }

        const products: Product[] = [];
        const productsError: string[] = [];
        for (const urlRequest of urls) {
            try {
                const response = await request(urlRequest);
                products.push(<Product>JSON.parse(response.body));
            } catch (err) {
                const urlSplit = urlRequest.split('/');
                const idProduct = urlSplit[urlSplit.length - 1];
                productsError.push(idProduct);
            }
        }
        return { products, productsError };
    };

    getProductById = async (idProduct: string): Promise<Product | Utils> => {
        const urlRequest = `${url}/${idProduct}`;
        try {
            const response = await request(urlRequest);
            const product = <Product>JSON.parse(response.body);
            product.sucess = true;
            return product;
        } catch (err) {
            return messageTreatment.errorMessageDefault('Falha ao buscar produto', err);
        }
    };
}

export const productIntegration = new ProductIntegration();