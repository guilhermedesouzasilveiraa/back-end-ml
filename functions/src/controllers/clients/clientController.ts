import { jwtController } from '../authentication/jwtController';
import { clientBusiness } from '../../business/exportBusiness';
import { Client } from '../../interfaces/exportInterfaces';

class ClientController {

    getClients = async (headers: any) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? await clientBusiness.getClients() : validate;
    };

    getClientById = async (headers: any, idClient: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? clientBusiness.getClientById(idClient) : validate;
    };

    createClient = async (headers: any, client: Client) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? clientBusiness.createClient(client) : validate;
    };

    updateClient = async (headers: any, client: Client) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? clientBusiness.updateClient(client) : validate;
    };

    removeClient = async (headers: any, idClient: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? clientBusiness.removeClient(idClient) : validate;
    };
}

export const clientController = new ClientController();