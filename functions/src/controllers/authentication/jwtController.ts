import * as redis from 'redis';
import JWTR from 'jwt-redis';
const redisClient = redis.createClient();
const jwtr = new JWTR(redisClient);

import { OptionsToken } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';
import { appSettings } from '../../../config/appSettings';

const key = appSettings.optionsToken.key;
const optionsToken: OptionsToken = {
    expiresIn: appSettings.optionsToken.expiresIn
};

class JwtController {
    createToken = async (payload: any) => {
        try {
            return await jwtr.sign(payload, key, optionsToken);
        }
        catch (err) {
            return err;
        }
    };

    decodedToken = async (token: string) => {
        try {
            const decoded = {
                sucess: true,
                decoded: await jwtr.verify(token, key)
            };
            return decoded;
        } catch (err) {
            return err;
        }
    };

    destroyToken = async (jti: string) => {
        try {
            await jwtr.destroy(jti);
            return messageTreatment.sucessMessageDefault('Token destuído e logout com sucesso');
        } catch (err) {
            return messageTreatment.errorMessageDefault('Não foi possível destuir o token atual', err);
        }
    };

    validateHeader = async (headers: any) => {
        const decoded = await this.decodedToken(headers['token']);
        if (!decoded.sucess) {
            return messageTreatment.errorMessageDefault('Não foi possível decodificar o token', decoded);
        }
        return decoded;
    };
}

export const jwtController = new JwtController();