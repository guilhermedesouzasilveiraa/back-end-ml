import { jwtController } from '../..//controllers/authentication/jwtController';
import { authenticationBusiness } from '../../business/exportBusiness';

class AuthenticationController {
    authenticate = (email: string, name: string) => {
        return authenticationBusiness.authenticateByEmailAndName(email, name);
    };

    logout = async (headers: any) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? authenticationBusiness.logout(validate.decoded.jti) : validate;
    };
}

export const authenticationController = new AuthenticationController();