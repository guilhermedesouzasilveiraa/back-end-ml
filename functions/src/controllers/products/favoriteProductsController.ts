import { jwtController } from '../authentication/jwtController';
import { favoriteProductsBusiness } from '../../business/exportBusiness';

class FavoriteProductsController {

    getListFavoriteProducts = async (headers: any) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.getListFavoriteProducts() : validate;
    };

    getListFavoriteProductsByIdClient = async (headers: any, idClient: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.getListFavoriteProductsByIdClient(idClient) : validate;
    };

    createListFavorite = async (headers: any, listFavorite: any) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.createListFavorite(listFavorite) : validate;
    };

    pushProductListFavorite = async (headers: any, id: string, idProduct: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.pushProductListFavorite(id, idProduct) : validate;
    };

    pullProductListFavorite = async (headers: any, id: string, idProduct: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.pullProductListFavorite(id, idProduct) : validate;
    };

    removeListFavorite = async (headers: any, id: string) => {
        const validate = await jwtController.validateHeader(headers);
        return validate.sucess ? favoriteProductsBusiness.removeListFavorite(id) : validate;
    };
}

export const favoriteProductsController = new FavoriteProductsController();