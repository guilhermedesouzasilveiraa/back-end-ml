import { Utils } from "../interfaces/exportInterfaces";

class MessageTreatment {
    errorMessageDefault = (message: string, err?: any): Utils => {
        const msg = `Erro: - ${message}${err ? (' - ' + err) : ''}`;
        console.log(msg);
        return <Utils>{ message: msg, status: 400 };
    };

    infoMessageDefault = (message: string, info?: any): Utils => {
        const msg = `Informativo - ${message}${info ? (' - ' + info) : ''}`;
        console.log(msg);
        return <Utils>{ message: msg, status: 250 };
    };

    sucessMessageDefault = (message: string, sucess?: any): Utils => {
        const msg = `Sucesso - ${message}${sucess ? (' - ' + sucess) : ''}`;
        console.log(msg);
        return <Utils>{ message: msg, status: 200 };
    };
}

export const messageTreatment = new MessageTreatment();