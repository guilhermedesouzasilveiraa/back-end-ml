export interface Product {
    id: string;
    title: string;
    image: string;
    brand: string;
    price: number;
    reviewScore: number;
    sucess: true;
}