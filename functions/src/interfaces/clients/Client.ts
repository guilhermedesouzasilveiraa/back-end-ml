import { Product } from '../exportInterfaces';

export interface Client {
    id: string;
    name: string;
    email: string;
    productsDetail: Product[];
    sucess: true;
}