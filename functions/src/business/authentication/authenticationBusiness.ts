import { clientBusiness } from '../../business/exportBusiness';
import { jwtController } from '../../controllers/exportControllers';

class AuthenticationBusiness {
    authenticateByEmailAndName = async (email: string, name: string) => {
        const client = await clientBusiness.getClientByEmailAndName(email, name);

        const token = await jwtController.createToken({ ...client });

        return { token: token };
    };

    logout = async (jti: string) => {        
        return await jwtController.destroyToken(jti);
    }
}

export const authenticationBusiness = new AuthenticationBusiness();