import * as _ from 'lodash';
import { favoriteProductDatasource } from '../../datasource/exportDatasources';
import { productIntegration } from '../../integrations/exportIntegrations'
import { FavoriteList } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';
import { clientBusiness } from '../exportBusiness';

class FavoriteProductsBusiness {

    getListFavoriteProducts = () => {
        return favoriteProductDatasource.getListFavoriteProducts();
    };

    getListFavoriteProductsByIdClient = async (idClient: string) => {
        const client = await clientBusiness.getClientById(idClient);

        if (!client.sucess) {
            return messageTreatment.errorMessageDefault('Cliente não encontrado, informe um cliente válido');
        }

        return favoriteProductDatasource.getListFavoriteProductsByIdClient(idClient);
    };

    createListFavorite = async (favoriteList: FavoriteList) => {
        const client = await clientBusiness.getClientById(favoriteList.idClient);

        if (!client.sucess) {
            return messageTreatment.errorMessageDefault('Cliente não encontrado, informe um cliente válido');
        }

        const favList = await favoriteProductDatasource.getListFavoriteProductsByIdClient(favoriteList.idClient);

        if (favList.sucess) {
            return messageTreatment.infoMessageDefault('O cliente já possui uma lista de favoritos');
        }

        if (favoriteList.products.length) {
            const productsDetail = await productIntegration.getProductsByIds(favoriteList.products);

            if (productsDetail.productsError.length) {
                _.pullAll(favoriteList.products, productsDetail.productsError);

                const favListAddRestriction = await favoriteProductDatasource.createListFavorite(favoriteList);

                return favListAddRestriction.sucess ?
                    messageTreatment.infoMessageDefault('Lista de favoritos adicionada com sucesso, sem os produto(s) não identificado(s):', productsDetail.productsError) : favListAddRestriction;
            }
        }

        const favListAddNoRestriction = await favoriteProductDatasource.createListFavorite(favoriteList);

        return favListAddNoRestriction.sucess ?
            messageTreatment.sucessMessageDefault('Lista de favoritos adicionada com sucesso') : favListAddNoRestriction;
    };

    pushProductListFavorite = async (id: string, idProduct: string) => {
        const product = await productIntegration.getProductById(idProduct);
        return product.sucess ? favoriteProductDatasource.pushProductListFavorite(id, idProduct) : messageTreatment.infoMessageDefault('Produto não existe na lista de produtos');
    };

    pullProductListFavorite = async (id: string, idProduct: string) => {
        return await favoriteProductDatasource.pullProductListFavorite(id, idProduct);
    };

    removeListFavorite = (id: string) => {
        return favoriteProductDatasource.removeListFavorite(id);
    };
}

export const favoriteProductsBusiness = new FavoriteProductsBusiness();