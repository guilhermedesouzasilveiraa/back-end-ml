import { clientDatasource, favoriteProductDatasource } from '../../datasource/exportDatasources';
import { productIntegration } from '../../integrations/exportIntegrations';
import { Client } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';

class ClientBusiness {

    getClients = async () => {
        const clients: Array<Client> = await clientDatasource.getClients();
        if (clients.length) {
            for (const [index, client] of clients.entries()) {
                const listFavorite = await favoriteProductDatasource.getListFavoriteProductsByIdClient(client.id);
                if (listFavorite && listFavorite.sucess && listFavorite.products) {
                    const productsDetail = await productIntegration.getProductsByIds(listFavorite.products);
                    clients[index].productsDetail = productsDetail.products;
                }
            }
        }
        return clients;
    };

    getClientById = async (idClient: string) => {
        const client = await clientDatasource.getClientById(idClient);
        if (client.sucess) {
            const listFavorite = await favoriteProductDatasource.getListFavoriteProductsByIdClient(client.id);
            if (listFavorite && listFavorite.sucess && listFavorite.products) {
                const productsDetail = await productIntegration.getProductsByIds(listFavorite.products);
                client.productsDetail = productsDetail.products;
            }
        }
        return client;
    };

    getClientByEmailAndName = async (email: string, name: string) => {
        return await clientDatasource.getClientByEmailAndName(email, name);
    };

    createClient = async (client: Client) => {
        const clientExists = await clientDatasource.getClientByEmailAndName(client.email);
        return clientExists.sucess ? messageTreatment.infoMessageDefault('Cliente existente na base de dados') : clientDatasource.createClient(client);
    };

    updateClient = (client: Client) => {
        const idClient = client.id;
        delete client.id;
        return clientDatasource.updateClient(idClient, client);
    };

    removeClient = (idClient: string) => {
        return clientDatasource.removeClient(idClient);
    };
}

export const clientBusiness = new ClientBusiness();