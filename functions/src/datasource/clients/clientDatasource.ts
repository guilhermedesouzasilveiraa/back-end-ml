import * as admin from 'firebase-admin';
const firestore = admin.firestore();

import { Client, Utils } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';

class ClientDatasource {

    getClients = async (): Promise<Array<Client> | any> => {
        const collection = firestore.collection('clients');
        return await collection
            .get()
            .then(async (snapshot) => {
                const clients: Array<Client> = [];
                snapshot.forEach((doc) => {
                    const client = <Client>doc.data();
                    client.id = doc.id;
                    clients.push(client);
                });
                return clients;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault('Erro ao buscar lista clientes, tente novamente', err);
            });
    };

    getClientById = async (idClient: string): Promise<Client | Utils> => {
        const collection = firestore.collection('clients');
        return await collection.doc(idClient)
            .get()
            .then(async (doc) => {
                const client = <Client>doc.data();
                client.id = doc.id;
                client.sucess = true;
                return client;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault('Erro ao buscar cliente por id, tente novamente', err);
            });
    };

    getClientByEmailAndName = async (email: string, name?: string): Promise<Client | Utils> => {
        let collection = firestore.collection('clients').where('email', '==', email);
        if (name) {
            collection = collection.where('name', '==', name);
        }
        return await collection.get()
            .then(async (snapshot): Promise<Client> => {
                let client = <Client>{};
                snapshot.forEach((doc) => {
                    const cli = {
                        id: doc.id,
                        email: doc.data().email,
                        name: doc.data().name,
                        sucess: true
                    };
                    client = <Client>cli;
                });
                return client;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault('Erro ao buscar cliente por email e nome, tente novamente', err);
            });
    };

    createClient = async (client: Client): Promise<Utils> => {
        return await firestore.runTransaction(async transaction => {
            const documentRef = firestore.collection('clients').doc();
            try {
                const doc = await transaction.get(documentRef);
                if (doc.exists) {
                    return messageTreatment.infoMessageDefault(`Client ${client.email} já existe`);
                } else {
                    transaction.create(documentRef, client);
                    return messageTreatment.sucessMessageDefault(`Client ${client.email} adicionado`);
                }
            }
            catch (err) {
                return messageTreatment.errorMessageDefault('Erro ao criar o cliente, tente novamente', err);
            }
        });
    };

    updateClient = async (idClient: string, client: Client): Promise<Client | Utils> => {
        return await firestore.runTransaction(async transaction => {
            const documentRef = firestore.doc(`clients/${idClient}`);
            try {
                const doc = await transaction.get(documentRef);
                if (doc.exists) {
                    transaction.update(documentRef, client);
                    client.sucess = true;
                    return client;
                }
                else {
                    return messageTreatment.infoMessageDefault(`Cliente ${idClient} não existe`);
                }
            }
            catch (err) {
                return messageTreatment.errorMessageDefault('Erro ao alterar o cliente, tente novamente', err);
            }
        });
    };

    removeClient = async (idClient: string): Promise<Utils> => {
        const collection = firestore.collection('clients');

        const client = await this.getClientById(idClient);

        if (!client.sucess) {
            return messageTreatment.errorMessageDefault(`Cliente ${idClient} não encontrado`);
        }

        return await collection.doc(idClient)
            .delete()
            .then(() => {
                return messageTreatment.sucessMessageDefault(`Cliente ${idClient} excluído`);
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault(`Não foi possível excluir o cliente ${idClient}, tente novamente`, err);
            });
    };
}

export const clientDatasource = new ClientDatasource();