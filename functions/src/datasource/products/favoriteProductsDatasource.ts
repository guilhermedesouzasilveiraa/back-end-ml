import * as admin from 'firebase-admin';
const firestore = admin.firestore();
import * as _ from 'lodash';

import { FavoriteList, Utils } from '../../interfaces/exportInterfaces';
import { messageTreatment } from '../../utils/MessageTreatment';

class FavoriteProductDatasource {

    getListFavoriteProducts = async (): Promise<FavoriteList[] | Utils> => {
        const collection = firestore.collection('listFavoriteProducts');
        return await collection
            .get()
            .then(async (snapshot) => {
                const listFavorite: Array<any> = [];
                snapshot.forEach((doc) => {
                    const product = <FavoriteList>doc.data();
                    product.id = doc.id;
                    listFavorite.push(product);
                });
                return listFavorite;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault('Erro ao buscar lista de favoritos, tente novamente', err);
            });
    };

    getListFavoriteProductsByIdClient = async (idClient: string): Promise<FavoriteList | Utils> => {
        const collection = firestore.collection('listFavoriteProducts');
        return await collection
            .where('idClient', '==', idClient)
            .get()
            .then(async (snapshot): Promise<FavoriteList> => {
                let listFavorite = <FavoriteList>{};
                snapshot.forEach((doc) => {
                    const list = <FavoriteList>doc.data();
                    list.id = doc.id;
                    list.sucess = true;
                    listFavorite = list;
                });
                return listFavorite;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault(`Erro ao buscar lista de favoritos do cliente ${idClient}`, err);
            });
    };

    createListFavorite = async (favoriteList: FavoriteList): Promise<FavoriteList | Utils> => {
        return await firestore.runTransaction(async transaction => {
            const documentRef = firestore.collection('listFavoriteProducts').doc();
            await transaction.get(documentRef);
            transaction.create(documentRef, favoriteList);
            favoriteList.sucess = true;
            return favoriteList;
        }).catch((err) => {
            return messageTreatment.errorMessageDefault('Erro ao criar lista de favoritos, tente novamente', err);
        });
    };

    pushProductListFavorite = async (id: string, idProduct: string): Promise<Utils> => {
        const documentRef = firestore.collection('listFavoriteProducts').doc(id);

        const productExists = await this.productExistsListFavorite(documentRef, idProduct);

        if (productExists.sucess === true) {
            return messageTreatment.infoMessageDefault(`Produto ${idProduct} já existe na lista de favoritos`);
        } else if (productExists.sucess === false) {
            return documentRef.update({
                products: admin.firestore.FieldValue.arrayUnion(idProduct)
            })
                .then(() => {
                    return messageTreatment.sucessMessageDefault(`Produto ${idProduct} adicionado com sucesso`);
                })
                .catch((err) => {
                    return messageTreatment.errorMessageDefault('Falha ao adicionar produto da lista de favoritos', err);
                });
        } else {
            return <Utils>productExists;
        }

    };

    productExistsListFavorite = async (documentRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>, idProduct: string) => {
        return await documentRef.get()
            .then(async (doc) => {
                const products = await doc.get('products');
                const productExists = _.find(products, (product) => {
                    return product === idProduct;
                });
                return productExists ? { sucess: true } : { sucess: false };
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault('Falha ao buscar produto na lista de favoritos', err);
            });
    }

    pullProductListFavorite = async (id: string, idProduct: string) => {
        const documentRef = firestore.collection('listFavoriteProducts').doc(id);

        const productExists = await this.productExistsListFavorite(documentRef, idProduct);

        if (productExists.sucess === true) {
            return documentRef.update({
                products: admin.firestore.FieldValue.arrayRemove(idProduct)
            })
                .then(() => {
                    return messageTreatment.sucessMessageDefault(`Produto ${idProduct} removido com sucesso`);
                })
                .catch((err) => {
                    return messageTreatment.errorMessageDefault('Falha ao remover produto da lista de favoritos', err);
                });
        } else if (productExists.sucess === false) {
            return messageTreatment.infoMessageDefault(`Produto ${idProduct} não existe na lista de favoritos`);
        } else {
            return <Utils>productExists;
        }
    };

    removeListFavorite = async (id: string): Promise<Utils> => {
        const collection = firestore.collection('listFavoriteProducts');

        const removeList = await this.getListFavoriteById(id);

        if (!removeList.sucess) {
            return messageTreatment.errorMessageDefault(`Lista de favoritos ${id} não encontrada`);
        }

        return collection
            .doc(id)
            .delete()
            .then(() => {
                return messageTreatment.sucessMessageDefault(`Lista de favoritos ${id} excluída`);
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault(`Não foi possível excluir a lista ${id}, tente novamente`, err);
            });

    };

    getListFavoriteById = async (id: string): Promise<FavoriteList | Utils> => {
        const collection = firestore.collection('listFavoriteProducts');
        return await collection
            .doc(id)
            .get()
            .then((doc) => {
                let listFavorite = <FavoriteList>{};
                listFavorite = <FavoriteList>doc.data();
                listFavorite.sucess = true;
                return listFavorite;
            })
            .catch((err) => {
                return messageTreatment.errorMessageDefault(`Não foi possível encontrar a lista ${id}`, err);
            });
    };
}

export const favoriteProductDatasource = new FavoriteProductDatasource();