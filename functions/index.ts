import * as admin from 'firebase-admin';
const serviceAccount = require('../../functions/config/back-end-ml-firebase-adminsdk-27q75-e01d89e268.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://back-end-ml.firebaseio.com"
});

import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';
import { clientController, favoriteProductsController, authenticationController } from './src/controllers/exportControllers';

/* APPS */
const api = express();
const appAuthentication = express();
const appClient = express();
const appFavoriteProducts = express();


/* CORS */
api.use(cors({ origin: true }));
appAuthentication.use(cors({ origin: true }));
appClient.use(cors({ origin: true }));
appFavoriteProducts.use(cors({ origin: true }));


/* ROTA -> /api */
api.get('/', async (req, res) => {
    res.send(`STATUS: ONLINE ${new Date()}`);
});

/* ROTA -> /Authentication */
appAuthentication.route('/authenticate').post(async (req, res) => { res.json(await authenticationController.authenticate(req.body.email, req.body.name)) })
appAuthentication.route('/logout').post(async (req, res) => { res.json(await authenticationController.logout(req.headers)) });

/* ROTA -> /client */
appClient.get('/:idClient', async (req, res) => {res.json(await clientController.getClientById(req.headers, req.params.idClient));
});
appClient.route('/')
    .get(async (req, res) => { res.json(await clientController.getClients(req.headers)) })
    .post(async (req, res) => { res.json(await clientController.createClient(req.headers, req.body)) })
    .put(async (req, res) => { res.json(await clientController.updateClient(req.headers, req.body)) })
    .delete(async (req, res) => { res.json(await clientController.removeClient(req.headers, req.body.id)) });


/* ROTA -> /favoriteProducts */
appFavoriteProducts.get('/:idClient', async (req, res) => { res.json(await favoriteProductsController.getListFavoriteProductsByIdClient(req.headers, req.params.idClient)) });
appFavoriteProducts.put('/push', async (req, res) => { res.json(await favoriteProductsController.pushProductListFavorite(req.headers, req.body.id, req.body.idProduct)) });
appFavoriteProducts.put('/pull', async (req, res) => { res.json(await favoriteProductsController.pullProductListFavorite(req.headers, req.body.id, req.body.idProduct)) });
appFavoriteProducts.route('/')
    .get(async (req, res) => { res.json(await favoriteProductsController.getListFavoriteProducts(req.headers)) })
    .post(async (req, res) => { res.json(await favoriteProductsController.createListFavorite(req.headers, req.body)) })
    .delete(async (req, res) => { res.json(await favoriteProductsController.removeListFavorite(req.headers, req.body.id)) });


/* EXPORTS */
exports.api = functions.https.onRequest(api);
exports.authentication = functions.https.onRequest(appAuthentication);
exports.client = functions.https.onRequest(appClient);
exports.favoriteProducts = functions.https.onRequest(appFavoriteProducts);