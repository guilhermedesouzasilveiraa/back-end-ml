export const appSettings = {
    integrations: {
        product: {
            url: 'http://challenge-api.luizalabs.com/api/product'
        }
    },
    optionsToken: {
        key: 'GuilhermeDeSouzaSilveira',
        expiresIn: 180000
    }
};